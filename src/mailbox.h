#pragma once

#include <httplib.h>
#include <string>
#include <map>
#include <unordered_map>

using namespace httplib;

class Mailbox
{
public:
	static void getAll(const Request &req, Response &res);
    static void getAllUnread(const Request &req, Response &res);
    static void sendMail(const Request &req, Response &res);
    static void getEmailById(const Request &req, Response &res);
    static void deleteEmailById(const Request &req, Response &res);
    static void getEmailsBySender(const Request &req, Response &res);
    static void getStatistic(const Request &req, Response &res);



private:
    static std::map<unsigned int, std::map<std::string, std::string>> mails;
	static std::mutex reqMutex;
    static unsigned long nextId;
};
