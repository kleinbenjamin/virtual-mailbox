#include <QCoreApplication>
#include <iostream>
#include <httplib.h>
#include "mailbox.h"

using namespace httplib;

using namespace std;
int main(int argc, char *argv[])
{
     QCoreApplication a(argc, argv);

     Server server;

     server.Get(R"(/get_all)", Mailbox::getAll);
     server.Get(R"(/get_all_unread)", Mailbox::getAllUnread);
     server.Post(R"(/send_email)", Mailbox::sendMail);
     server.Get(R"(/get_by_id/(\d+))", Mailbox::getEmailById);
     server.Delete(R"(/delete_by_id/(\d+))", Mailbox::deleteEmailById);
     server.Get(R"(/get_emails_by_sender/(\w+))", Mailbox::getEmailsBySender);
     server.Get(R"(/get_statistic)", Mailbox::getStatistic);

     server.listen("0.0.0.0", 7373);

}
