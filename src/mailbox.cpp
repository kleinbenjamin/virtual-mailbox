#include "mailbox.h"
#include <iostream>
#include <json.hpp>
#include <QString>

using namespace nlohmann;

 std::mutex Mailbox::reqMutex;
 unsigned long Mailbox::nextId = 0;
 std::map<unsigned int, std::map<std::string, std::string>> Mailbox::mails;


 // Get All Emails
void Mailbox::getAll(const Request &request, Response &response)
{
    std::lock_guard<std::mutex> lock(reqMutex);

    json resBody = json::array({});

    for(const auto &mail : mails) {
        resBody.push_back({
            {"id", mail.first},
            {"body", mail.second}
        });
        mails[mail.first].at("read")= "true";
    }
    response.set_content(resBody.dump(), "application/json");
}

void Mailbox::getAllUnread(const Request &req, Response &res) {
    std::lock_guard<std::mutex> lock(reqMutex);
    json resBody = json::array({});

    for(const auto &mail : mails) {
       if(mail.second.at("read") == "false") {
           resBody.push_back({
                {"id", mail.first},
                {"body", mail.second}
           });
           mails[mail.first].at("read")= "true";
       }
    }
    res.set_content(resBody.dump(), "application/json");
}


// Send one Email
void Mailbox::sendMail(const Request &request, Response &response) {
    std::lock_guard<std::mutex> lock(reqMutex);
    try {
        json reqBody = json::parse(request.body);

        auto content = reqBody.at("body");
        if(content.find("sender") != content.end() && content.find("subject") != content.end() && content.find("content") != content.end()) {
            std::cout << reqBody.at("body");

            json j = {
                {"sender", content.at("sender")},
                {"subject", content.at("subject")},
                {"content", content.at("content")},
                {"read", "false"}
            };

            mails.emplace(Mailbox::nextId, j);

            json resBody = {
                {"id", Mailbox::nextId},
            };
            ++Mailbox::nextId;
            response.set_content(resBody.dump(), "application/json");
            std::cout << "SIker\n";
        } else {
            std::cout << "nincs\n\n";
        }
    } catch (...) {
        response.status = 400;
    }
}

void Mailbox::getEmailById(const Request &request, Response &response) {
    std::lock_guard<std::mutex> lock(reqMutex);
        auto reqId = request.matches[1];
        try {
            const unsigned int id = std::stoul(reqId);
            json resBody = json::array({});

            for(const auto &mail : mails) {
                std::cout << mail.first << std::endl << id;
                if(mail.first == id) {
                    resBody.push_back({
                        {"id", mail.first},
                        {"body", mail.second}
                    });
                    mails[mail.first].at("read")= "true";
                }
            }

            response.set_content(resBody.dump(), "application/json");
        }  catch (...) {
            std::cout << "nemjo\n";
        } {}
}

void Mailbox::deleteEmailById(const Request &req, Response &res) {
    std::lock_guard<std::mutex> lock(reqMutex);

    auto reqId = req.matches[1];
    try {
        const unsigned int id = std::stoul(reqId);

        if(mails.find(id) == mails.end()) {
            std::cout << "nem létezik teson\n";

        } else {

            mails.erase(mails.find(id));
        }

        json resBody = json::array({});


        res.set_content(resBody.dump(), "application/json");
    }  catch (...) {
        std::cout << "nemjo\n";
    } {}
}

void Mailbox::getEmailsBySender(const Request &req, Response &res) {
    std::lock_guard<std::mutex> lock(reqMutex);

    std::string reqId = req.matches[1];
    try {
        std::string sender = reqId;

        json resBody = json::array({});

        for(const auto &mail : mails) {
            if(mail.second.at("sender") == sender) {
                resBody.push_back({
                    {"id", mail.first},
                    {"body", mail.second}
                });
                mails[mail.first].at("read")= "true";
            }
        }

        res.set_content(resBody.dump(), "application/json");
    }  catch (...) {
        std::cout << "nemjo\n";
    } {}
}

void Mailbox::getStatistic(const Request &req, Response &res)
{
    std::lock_guard<std::mutex> lock(reqMutex);

    json resBody = json::array({});

    std::map<std::string, int> sumOfMailsPerSender;
    for(const auto &mail : mails) {
        std::string sender = mail.second.at("sender");
        if(sumOfMailsPerSender.find(sender) == sumOfMailsPerSender.end()) {
            sumOfMailsPerSender.emplace(sender, 1);
        } else {
            int sum = sumOfMailsPerSender.at(sender);
            sumOfMailsPerSender.at(sender) = sum+1;
        }
    }

    for(const auto &s : sumOfMailsPerSender) {
        resBody.push_back({
            {"sender", s.first},
            {"sumOfMails", s.second}
        });
    }


    res.set_content(resBody.dump(), "application/json");
}
